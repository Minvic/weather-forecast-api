const net = require('net');
const geoip = require('fast-geoip');

/**
 * Status messages.
 * @private
 * @constant
 */
const VALIDATION_MSG = {
    ipError: 'The IP is not valid'
};

/**
 * Validates a IP string.
 * @private
 * @param {string} ip String to be validated as IP.
 * @returns {number} 0 for invalid strings, returns 4 for IP version 4 addresses,
 * and returns 6 for IP version 6 addresses.
 */
const validIP = (ip) => net.isIP(ip);

/**
 * Passing a valid IP, gets all its geo location data.
 * @param {string} ip Valid IP.
 * @returns {Promise<string | ipInfo>} All the ip geodata or an error message.
 */
const getIPLocation = async (ip) => !validIP ? VALIDATION_MSG.ipError : await geoip.lookup(ip);

module.exports = {
    getIPLocation,
}