<div align="center">
<h1 align="center">Weather Forecast API</h1>
  <p align="center">
    Simple API to gets the current weather with an IP
  </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## About The Project

This is a small project to test my abilities. It's ready to add new APIs but now only have one.
The available API is able of get the current weather passing it an IP address and getting its location.

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [Node.js](https://nodejs.org/en/) (16.14.0)
* [Fast-Geoip](https://github.com/onramper/fast-geoip/)
* [Expressjs](https://expressjs.com/)
* [Axios](https://github.com/axios/axios/)
* [Nodemon](https://github.com/remy/nodemon/)

<p align="right">(<a href="#top">back to top</a>)</p>

## Getting Started

It can't be more simple to install the project.

### Installation

1. Get an API Key at [OpenWeather](https://openweathermap.org/price)
2. Clone the repo
   ```sh
   git clone https://gitlab.com/Minvic/weather-forecast-api.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Creates the dotenv file using the .env.example file.
   ```
   API_KEY='ENTER YOUR KEY';
   ```
5. (Optional) You can change the port used by the API setting the `PORT` variable in your dotenv file.

<p align="right">(<a href="#top">back to top</a>)</p>

## Usage

To run the API just need to use the following command:
```sh
npm run start
```

<p align="right">(<a href="#top">back to top</a>)</p>

## Contact

Victor Esteban - [LinkedIn](https://www.linkedin.com/in/v%C3%ADctor-esteban-carqu%C3%A9s/) - minvic2@gmail.com

Project Link: [https://gitlab.com/Minvic/weather-forecast-api](https://gitlab.com/Minvic/weather-forecast-api)

<p align="right">(<a href="#top">back to top</a>)</p>
