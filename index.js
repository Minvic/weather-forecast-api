const express = require('express');
const utils = require('./utils');
const API = require('./API');

const app = express();
const PORT = utils.env.PORT();

app.get('/weather/:ip', async (req, res) => {
    const { ip } = req.params;

    if (!ip) {
        res.json({
            statusCode: 400,
            statusMsg: "IP not provided"
        });
    }

    const weatherData = await API.getWeatherByIP(ip);
    res.json(weatherData);
});

app.listen(PORT, () => {
    utils.logger.success(`http://localhost:${PORT}`);
})