const axios = require('axios').default;
const utils = require('../utils');

/**
 * All the calls to every needed Open Weather's API.
 */
const OPEN_WEATHER_API = {
    getCurrentWeather: (lat, lon, API_KEY) => (
        `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${API_KEY}`
    ),
};

/**
 * Gets the current weather in a Earth's coordinates.
 * Uses the OpenWeather API.
 * @param {number[]} coordinates Earth's latitude and longitude coordinates.
 * @returns {Promise<AxiosResponse | AxiosError>} Axios response data or Axios response error.
 */
const currentWeather = ([lat, lon]) => {
    const API_KEY = utils.env.API_KEY();
    const apiString = OPEN_WEATHER_API.getCurrentWeather(lat, lon, API_KEY);

    return new Promise((resolve, reject) => {
        axios
            .get(apiString)
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
}

module.exports = {
    currentWeather,
}