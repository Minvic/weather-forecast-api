const geoIP = require('../GeoIP');
const openWeather = require('../OpenWeather');

/**
 * Gets the IP location current weather data.
 * @param {string} ip Valid IP address.
 * @returns {object} Weather data or an error with status code and msg.
 */
const getWeatherByIP = async (ip) => {
    const locationData = await geoIP.getIPLocation(ip);
    
    if (!locationData) {
        return {
            statusCode: 404,
            statusMsg: "The IP is not exists or the IP location can't be found",
        };
    }

    const weatherData = await openWeather.currentWeather(locationData.ll);
    return weatherData;
}

module.exports = {
    getWeatherByIP,
};