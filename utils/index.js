module.exports = {

    /**
     * Utility to get environment variables.
     */
    get env() {
        return require('./env');
    },

    /**
     * Utility to colour the console output.
     */
    get logger() {
        return require('./logger');
    }
}