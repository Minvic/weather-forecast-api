require('dotenv').config();

const { logger } = require('.');

module.exports = {

    /**
     * Gets the current port used by the application.
     * @param {boolean} ignoreEnv Specifies if the dotenv is ignored.
     * @returns {string} Port used by the application.
     */
    PORT(ignoreEnv = false) {
        return ignoreEnv ? '8080' : process.env.PORT ?? '8080';
    },

    /**
     * Gets the API_KEY used by OpenWeather API.
     * If it's not set correctly in .env file, it will throw an error.
     * @returns {string} API_KEY used by OpenWeather API
     */
    API_KEY() {
        if (!process.env.API_KEY) {
            throw new Error(logger.error('API_KEY env is not set correctly'));
        }
        return process.env.API_KEY;
    },
}