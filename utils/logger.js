const {
    red,
    cyan,
    green,
    yellow,
    bold,
    italic,
    dim 
  } = require("colorette");
  
  module.exports = {
    help: (msg) => console.info(italic(yellow(dim(`🟣 - ${msg}`)))),
    error: (msg) => console.error(bold(red(`🔴 - ${msg}`))),
    info: (msg) => console.info(cyan(`🔵 - ${msg}`)),
    log: (msg) => console.log(msg),
    success: (msg) => console.log(bold(green(`🟢 - ${msg}`))),
    warn: (msg) => console.warn(yellow(`🟠 - ${msg}`)),
    raw: (msg) => console.log(msg),
  }